package MyPack2;

import java.util.*;


public class Mission2 {

    public static void main(String[] args) {

/*
        System.out.println("Задание 1");

        int[] arr = new int[20];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) Math.pow(2, i + 1);

        }
        System.out.println(Arrays.toString(arr));
        System.out.println(arr.length);
*/      //Задание 1

/*
        System.out.println();
        System.out.println("Задание 2");

        int[] arr = new int[11];
        arr[0] = 1;
        arr[1] = 1;
        for (int i = 2; i < arr.length; i++) {
            arr[i] = arr[i-1] + arr[i-2];
        }
        System.out.println(Arrays.toString(arr));
*/      //Задание 2

/*
        System.out.println();
        System.out.println("Задание 3");
*/      //Задание 3 (Шапка)

/*
      List<Integer> list = new ArrayList<>();
*/      //Требуется раскомментировать строчку для проверки подпунктов задания 3

/*
        list.add(1);
        list.add(3);
        list.add(7);
        list.add(2);
        list.add(6);
        list.add(1);
        list.add(1);
*/      //Вариант А1 заполнение вручную

/*
        int value;
        for (int i = 0; i < 10; i++) {
           list.add(i * 2);
        }
*/      //Вариант А2 заполнение через цикл

/*
 for (int arr: list) {
            System.out.print(" " + arr);
        }
        System.out.println();
*/      //Пункт Б

/*
       list.set(4, 21);
*/      //Пункт В

/*
        int index = list.size() - 1;
        list.remove(index);
        for (int arr1: list) {
            System.out.print(" " + arr1);
        }
*/      //Пункт Г

/*
        System.out.println();
        System.out.println(list.size());
*/      // Пункт Д

/*
        System.out.println();

        int index1;
        for (int i = 0; i < 2; i++) {
            index1 = list.size() - 1;
            list.remove(index1);
        }
*/      //Вариант E1 удаление циклом

/*
        int index2 = list.size() - 1;
        int index3 = list.size() - 2;
        list.remove(index2);
        list.remove(index3);
        for (int arr2: list) {
            System.out.print(" " + arr2);
        }
        System.out.println();
*/      //Вариант Е2 удаление вручную

/*
        System.out.println();
        System.out.println("Задание 4");
        System.out.println();

        List<Integer> list4 = new ArrayList<>();
        Random rand = new Random();
        for (int i = 0; i < 10; i++) {
        list4.add(rand.nextInt(100));
        }
        System.out.print((Arrays.toString(list4.toArray())));//Вывод списка через .toArray
        System.out.println();
        System.out.println(list4.get(5));
*/      //Задание 4

/*
        System.out.println();
        System.out.println("Задание 5");
        Map<Integer, Integer> map = new HashMap<>();
        Random rand = new Random();
        for (int i = 0; i < 10; i++) {
            map.put(i, rand.nextInt(100));
        }
        Set<Integer> keys = map.keySet();
        Iterator<Integer> iterator = keys.iterator();
        while (iterator.hasNext()){
            System.out.print(" " + map.get(iterator.next()));
        }
        System.out.println();
        System.out.println(map.size());
*/      //Задание 5

/*

        System.out.println();
        System.out.println("Задание 6");
        LinkedList<Integer> link = new LinkedList<>();
        Random rand = new Random();
        for (int i = 0; i < 10; i++) {
            link.add(rand.nextInt(100));
            System.out.print(" " + link.get(i));
        }
        System.out.println();
        Iterator<Integer> iterator = link.iterator();
        while (iterator.hasNext()){
            System.out.print(" " + iterator.next());
        }
        System.out.println();                   // Задание 7
        System.out.println(link.getFirst());
        System.out.println(link.getLast());
*/      // Задание 6 + 7

    }
}
